'use strict';

var gulp = require('gulp')
  , jshint = require('gulp-jshint')
  , stylish = require('jshint-stylish')
  , shell = require('shelljs');

let paths = require('./config').paths;
let localMods = require('./config').localMods;

gulp.task('lint', () => {
  return gulp.src([paths.js.SRC, paths.js.PR_MOD_SRC])
    .pipe(jshint())
    .pipe(jshint.reporter(stylish));
});

gulp.task('watch', function() {
  gulp.watch([paths.js.SRC], ['lint']);
  gulp.watch([paths.js.PR_MOD_SRC], ['lint']).on('change', (file) => {
    doLocalModUpdate(file);
  });
});

// --- main task:
gulp.task('dev', ['watch', 'lint']);


// --- helpers:
function doLocalModUpdate (changed) {
  for(var i=0; i < localMods.length; i++)
    if(changed.path.indexOf(paths.js.PR_MOD_BASE + localMods[i]) !== -1)
      shell.exec('npm uninstall ' + localMods[i]);
  shell.exec('npm install');
}
