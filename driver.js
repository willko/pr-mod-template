#!/usr/bin/env node
'use strict';

var core  = require('core')
  , utils = require('utils');

//test modA
core.modA.run();
utils.modA.run();

//test modB
let modB  = require('core/modB')
  , modB2 = require('utils/modB');
modB.run();
modB2.run();



console.log('<done>');
