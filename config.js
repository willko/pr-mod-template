'use strict';

var depData = require('./package.json').dependencies;

var _paths = {
  js: { //build path data
      SRC: 'src/**/*js'
    , PR_MOD_SRC:  'private_modules/**/*js'
    , PR_MOD_BASE: 'private_modules/'
  }
};

//extract private_module names to Array
var _localMods = [];
Object.keys(depData).forEach((k) => {
  if(depData.hasOwnProperty(k))
    if(depData[k].indexOf('file:') !== -1)
      _localMods.push(k);
});

module.exports = { //the exported Object
    paths:      _paths
  , localMods:  _localMods
};
