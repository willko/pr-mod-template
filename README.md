### PR-MOD-TEMPLATE

A simple boiler plate program for messing around with non-hosted package.json defined private modules.

### Setup

```
npm install
```

_then..._

```
npm run watch
```

This will watch for file changes and lint the code. It also re-installs private modules if needed.

_then (in a new terminal window)..._ 

```
npm start
```

Running `npm start` should execute the `driver.js` file.

### Structure

```
./
  ├── README.md
  ├── config.js
  ├── driver.js
  ├── gulpfile.js
  ├── package.json
  └── private_modules/
      ├── core/
      │   ├── core.js
      │   ├── modA/
      │   │   └── index.js
      │   ├── modB/
      │   │   └── index.js
      │   └── package.json
      └── utils/
          ├── modA/
          │   └── index.js
          ├── modB/
          │   └── index.js
          ├── package.json
          └── utils.js
```

The `core` and `utils` folders are private modules (not hosted on any registery).  They both contain submodules that are not official modules (just helpers to each parent module).  

### Adding Modules

You can create additional or replace modules such as the `core` or `utils` modules, then install them with the following...

```
npm install ./private_modules/module_name --save
```

this will automatically add them to the project's root `package.json` file. Gulp will also watch these and re-install them on changes automagically.

_(look at `config.js` & `gulpfile.js` if you want to see how gulp does this)._

You can also just add some submodules without creating a new `package.json` defined module.  Just add a folder or file to the `core` directory and it will automatically be available through the `core` module. 

_(If curious on how this works, you can check out `core.js` in the core directory to see how the `core` module adds any added submodule files automatically to its library)._