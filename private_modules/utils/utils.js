'use strict';

var fs = require('fs')
  , mod = {};

//create an array of current dir file structure
var dirContents = fs.readdirSync(__dirname);

//strip this & non-sub-module files
dirContents.splice(dirContents.indexOf('utils.js'), 1);
dirContents.splice(dirContents.indexOf('package.json'), 1);

//add each sub-module to the mod object
dirContents.forEach((file) => {
  var name = file.replace(/[.]js/g, '');
  mod[name] = require('./' + file);
});

module.exports = mod;
