'use strict';

var fs = require('fs')
  , core_lib = {};

//create an array of current dir file structure
var dirContents = fs.readdirSync(__dirname);

//strip this & non-sub-module files
dirContents.splice(dirContents.indexOf('core.js'), 1);
dirContents.splice(dirContents.indexOf('package.json'), 1);

//add each sub-module to the core_lib object
dirContents.forEach((file) => {
  var name = file.replace(/[.]js/g, '');
  core_lib[name] = require('./' + file);
});

module.exports = core_lib;
